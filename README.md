# **Simulación de 2 partículas sometidas a un campo magnético constante**

### Autores: Kevin Andres Restrepo Tobón  *kevin.retrepot@udea.edu.co*, Samuel Durán Bustamante *samuel.duran@udea.edu.co*


Consideramos 2 particulas con cierta carga $q_1$, $q_2$, a las cuales someteremos a un campo magnético $\vec{B}$ constante y con las condiciones iniciales para las particulas de aceleración nula, velocidad no nula y las partículas iniciarán el movimiento acercandose.

El desarrollo del código consiste en usar la fuerza de Lorentz:

$$\vec{F_L}=q \hspace{0.2cm} \vec{v} \times \vec{B} \hspace{2cm} (1)$$

Junto a la ley de Coulomb:

$$  \vec{F_c}=K \frac{q_1q_2}{r^2} \hat{r} \hspace{2cm} (2) $$

Con el principio de superposición de las fuerzas, la fuerza total que actúa sobre las partículas es simplemente sumar (1) y (2), se despeja la aceleración de la partícula de la segunda ley de Newton $\vec{F}=m\vec{a}$, la velocidad $\vec{v}=\vec{v_0}+\vec{a}\hspace{0.05cm}t$ y la posición $\vec{x}=\vec{x_0}+\vec{v}\hspace{0.05cm}t+\frac{1}{2}\vec{a}t^2$ se obtienen iterando sobre pequeños intervalos de tiempo. Finalmente se grafican las componentes de la posicion, velocidad y aceleración de las partículas respecto al tiempo en conjunto con la trayectoria seguida por estas.

En el notebook graficas.ipynb se encuentran ejemplos de uso de la librería.

Este proyecto se esforzó mucho para nunca usar numpy arrays y se siente orgulloso al respecto.

## Referencias

[1]. Zangwill, A. (2019). Modern Electrodynamics. Cambridge University Press (Virtual Publishing). https://doi.org/10.1017/cbo9781139034777
