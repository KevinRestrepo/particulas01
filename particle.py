import matplotlib.pyplot as plt
from datetime import datetime

_k = 8.987551e9 #coulomb cte
_e = 1.6e-19  #elementary electric charge
_mp = 1.67e-27 #proton mass

class particle():
  """
  Representa una partícula puntual en el espacio.

  Atributos
  --------
  pos : list
    una lista con la posición actual de la partícula
  vel : list
    una lista con la velocidad actual de la partícula
  acel : list
    una lista con la aceleración actual de la partícula
  mass : float
    la masa del a partícula
  charge : float
    la carga de la partícula
  
  Métodos
  ------
  get_distance_sr(point)
    retorna la distancia cuadrado desde la posición de la partícula a un punto

  """
  def __init__(self,pos: list,vel: list,**kwargs):
      '''
      Parámetros
      ----------
      pos : list
        posición inicial de la partícula [x0,y0,z0]
      vel : list
        Velocidad inicial de la partícula
      mass : float, opcional
        la masa de la partícula (defecto es 1)
      charge : float, opcional
        la carga de la partícula (defecto es 1)
      '''
      self.pos = pos 
      self.vel = vel
      self.acel = [0,0,0]

      self.mass = kwargs.get("mass",1)
      self.charge = kwargs.get("charge",1)
  
  def get_distance_sr(self,point):
      '''
      Calcula la distancia al cuadrado entre esta partícula y un punto x,y,z del espacio

      Parámetros
      ---------
      point : list
        lista de 3 elementos con el punto en R3 [x,y,z]
      '''
      x,y,z = point
      x0,y0,z0 = self.pos

      return (x-x0)**2+(y-y0)**2+(z-z0)**2

  def _get_coulomb(self,particle,k = 1):
      '''
      Calcula la fuerza de coulomb entre esta instancia y otra partícula.
      '''
      q1 = self.charge
      q2 = particle.charge
      r_squared = self.get_distance_sr(particle.pos)

      unitx = (particle.pos[0]-self.pos[0])/r_squared**(1/2) #vector unitario en x
      unity = (particle.pos[1]-self.pos[1])/r_squared**(1/2) #vector unitario en y
      unitz = (particle.pos[2]-self.pos[2])/r_squared**(1/2) #vector unitario en z
      
      fcx=k*q1*q2*unitx/r_squared #Componente x de la fuerza de coulomb 
      fcy=k*q1*q2*unity/r_squared #Componente y de la fuerza de coulomb
      fcz=k*q1*q2*unitz/r_squared #Componente z de la fuerza de coulomb

      return [fcx,fcy,fcz]

  def _get_lorentz(self,field):
      '''
      Calcula la fuerza de Lorentz entre esta instancia y un campo magnético.
      '''
      q  = self.charge
      vx = self.vel[0]
      vy = self.vel[1]
      vz = self.vel[2]
      Bx = field.x
      By = field.y
      Bz = field.z
      
      flx=q*(vy*Bz-vz*By) #componente x de la fuerza de lorentz
      fly=q*(vz*Bx-vx*Bz) #componente y de la fuerza de lorentz
      flz=q*(vx*By-vy*Bx) #componente z de la fuerza de lorentz

      return [flx,fly,flz] 

  def _update_acel(self,particle = None,B = None):
      '''
      Actualiza la aceleración de la partícula despejando F = ma.
      '''
      m = self.mass

      #si hay particula 2, calcula la interacción, si no, retorna una fuerza cero
      if particle:
        fcx, fcy, fcz = self._get_coulomb(particle)
      else:
        fcx, fcy, fcz = [0,0,0]
      #si hay campo, calcula la interacción, si no, retorna una fuerza cero
      if B:
        flx, fly, flz = self._get_lorentz(B)
      else:
        flx, fly, flz = [0,0,0]

      #El signo negativo es debido a la definición de los vectores unitarios abajo
      acelx = (flx-fcx)/m  #Despejando la componente de la aceleración en x por la segunda ley de newton
      acely = (fly-fcy)/m  #Despejando la componente de la aceleración en y por la segunda ley de newton
      acelz = (flz-fcz)/m  #Despejando la componente de la aceleración en z por la segunda ley de newton

      self.acel = [acelx,acely,acelz]
  
  def _update_vel(self,t):
      '''
      Actualiza la velocidad usando v=v_0+a*t*2.
      '''
      #usa como valores iniciales el valor de velocidad y aceleración que tiene actualmente
      self.vel = [self.vel[i]+self.acel[i]*t for i in range(0,3)]
  
  def _update_pos(self,t):
      '''
      Actualiza la posición usando x=x_0+v*t+0.5a*t**2.
      '''
      #usa como valores iniciales la posición, velocidad y aceleración actuales
      self.pos = [self.pos[i]+self.vel[i]*t+0.5*self.acel[i]*t**2 for i in range(0,3)]
      

class field():
  """
  Representa un campo escalar en R3

  Atributos
  --------
  x : float
    componente x del campo
  y : float
    componente y del campo
  z : float
    componente z del campo
  """
  def __init__(self,*args):
      '''
      Una clase simple para un campo simple
      '''
      self.x, self.y, self.z = args

class simulate():
  """
  Contiene información sobre la dinámica del movimiento.

  Atributos
  --------
  time : list
    lista con los valores del tiempo
  pos1 : list
    lista con las posiciones de la partícula 1
  pos2 : list
    lista con las posiciones de la partícula 2
  vel1 : list
    lista con las velocidades de la partícula 1
  vel2 : list
    lista con las velocidades de la partícula 2
  acel1 : list
    lista con las aceleraciones de la partícula 1
  acel2 : list 
    lista con las aceleraciones de la partícula 2
  
  Métodos
  ------
  show_pos("x","y","z",save = False)
    muestra una gráfica de las posiciones de ambas partículas en las componentes especificada
  show_vel("x","y","z",save = False)
    muestra una gráfica de las velocidades de ambas partículas en las componentes especificadas
  show_acel("x","y","z",save = False)
    muestra una gráfica de las aceleraciones de ambas partículas en las componentes especificadas
  show_traject(save = False)
    muestra una gráfica 3d de la trayectoria de ambas partículas
  """

  def __init__(self,particle1: particle,particle2: particle,field: field,T: float,N = 10000, epsilon = 0.00005):
    ''' 
    Parámetros
    ----------
    particle1 : particle
      una instancia de la clase particle
    particle2 : particle
      una instancia de la clase particle
    field : field
      una instancia de la clase field
    T : float
      tiempo de simulación
    N : int, opcional
      número de datos de la simulación (defecto es 10000)
    epsilon : float, opcional
      magnitud mínima de la distancia entre las partículas al cuadrado (punto de choque), existe para evitar que las gráficas diverjan cuando las partículas están muy cerca.
    
    '''
    self.time = [0] #inicialización del array de tiempo
    dt = T/N  #intervalo infinitesimal de tiempo

    self.pos1 = [particle1.pos] #posición inicial de la partícula 1
    self.pos2 = [particle2.pos] #posición inicial de la partícula 2

    self.vel1 = [particle1.vel] #velocidad inicial de la partícula 1
    self.vel2 = [particle2.vel] #velocidad inicial de la partícula 2

    self.acel1 = [particle1.acel] #aceleración incial de la partícula 1
    self.acel2 = [particle2.acel] #aceleración incial de la partícula 2
    #empieza la simulación
    self._save_data(particle1,particle2,field,T,dt,epsilon) #Función de abajo

  def _save_data(self,particle1,particle2,field,T,dt,epsilon):
    ''' 
    Crea los array de los datos de posición, velocidad y aceleración de cada partícula en cada instante de tiempo
    '''
    #mientras el tiempo final no sea mayor que el tiempo de simulación, simula el movimiento y añade los datos a las listas
    while self.time[-1]<T:
      #actualiza la posición, velocidad y aceleración de las partículas
      self._update_particles(particle1,particle2,field,dt)
      #si la distancia entre las partículas es menor al epsilon, colisiona las partículas
      if particle1.get_distance_sr(particle2.pos)< epsilon: #Para evitar las divergencias en los gráficos
        break

      self.pos1 += [particle1.pos] 
      self.pos2 += [particle2.pos] 

      self.vel1 += [particle1.vel] 
      self.vel2 += [particle2.vel] 

      self.acel1 += [particle1.acel] 
      self.acel2 += [particle2.acel] 

      self.time += [self.time[-1]+dt] 
      
  
  def _update_particles(self,particle1,particle2,field,dt):
    ''' 
    Actualiza el estado dinámico de las partículas usando las ecuaciones de movimiento
    '''
    particle1._update_acel(particle2,field) #Actualiza la aceleración de la partícula 1
    particle2._update_acel(particle1,field) #Actualiza la aceleración de la partícula 2

    particle1._update_vel(dt) #Actualiza la velocidad de la partícula 1
    particle2._update_vel(dt) #Actualiza la velocidad de la partícula 2
    
    particle1._update_pos(dt) #Actualiza la posición de la partícula 1
    particle2._update_pos(dt) #Actualiza la posición de la partícula 2
  

  def show_pos(self,*args,save = False,**kwargs):
    ''' 
    Muestra una gráfica de las componentes de la posición de cada partícula.

    Si no se pasa ningún argumento muestra las 3 componentes en un subplot, si se pasa un argumento de la forma simulate.show_pos("x") o simulate.show_pos("x","z") muestra sólo las componentes "x" o "x" y "z" de la posición.

    Parámetros
    ---------
    save : bool, opcional
      establece si se guarda un .png de la gráfica en el directorio (defecto es False)

    '''
    
    #Definitivamente hay una mejor forma de hacer todo esto.
    dict = {"x":0,"y":1,"z":2}
    pos1_trans = list(zip(*self.pos1)) #transpone los arrays con las posiciones para ser más fácil de trabajar
    pos2_trans = list(zip(*self.pos2))
    #busca si el usuario introdujo un argumento en común con el diccionario, si lo hay, añade el argumento a una lista para solo graficar tales componentes
    keys = []
    for key in list(set(args).intersection(dict.keys())):
      keys += [key]
    #si no hay argumentos, grafica todas las componentes
    if len(keys)==0:
      keys = ['x','y','z']
      fig, axes = plt.subplots(1,3,figsize = (13,5),dpi = 150)
      for key,ax in zip(keys,axes):
        ax.plot(self.time,pos1_trans[dict[key]],"k",label = 'Partícula 1',**kwargs)
        ax.plot(self.time,pos2_trans[dict[key]],"b",label = 'Partícula 2',**kwargs)
        ax.set_ylabel(f'{key}')
        ax.set_xlabel('t')
        ax.set_title(f'Posición en {key}')
    else:
      fig, axes = plt.subplots(1,len(keys),figsize = (13,5),dpi = 150)
      if len(keys)==1:
        axes = [axes]
      for key,ax in zip(keys,axes):
        ax.plot(self.time,pos1_trans[dict[key]],"k",label = 'Partícula 1',**kwargs)
        ax.plot(self.time,pos2_trans[dict[key]],"b",label = 'Partícula 2',**kwargs)
        ax.set_ylim(-10,10)
        ax.set_ylabel(f'{key}')
        ax.set_xlabel('t')
        ax.set_title(f'Posición en {key}')


    plt.legend()
    plt.tight_layout()
    #si el usuario eligió guardar la imagen, la guarda
    if save:
      date = datetime.now()
      str = date.strftime("%d_%m_%y_%H%M%S")
      plt.savefig(f'Position_{str}.png',bbox_inches ='tight')



  def show_vel(self,*args,save = False,**kwargs):
    ''' 
    Muestra una gráfica de las componentes de la velocidad de cada partícula.

    Si no se pasa ningún argumento muestra las 3 componentes en un subplot, si se pasa un argumento de la forma simulate.show_vel("x") o simulate.show_vel("x","z") muestra sólo las componentes "x" o "x" y "z" de la velocidad.

    Parámetros
    ---------
    save : bool, opcional
      establece si se guarda un .png de la gráfica en el directorio (defecto es False)

    '''
    #exactamente el mismo procedimiento de show_pos
    dict = {"x":0,"y":1,"z":2}
    vel1_trans = list(zip(*self.vel1)) 
    vel2_trans = list(zip(*self.vel2))

    keys = []

    for key in list(set(args).intersection(dict.keys())):
      keys += [key]

    if len(keys)==0:
      keys = ['x','y','z']
      fig, axes = plt.subplots(1,3,figsize = (13,5),dpi = 150)
      for key,ax in zip(keys,axes):
        ax.plot(self.time,vel1_trans[dict[key]],"k",label = 'Partícula 1',**kwargs)
        ax.plot(self.time,vel2_trans[dict[key]],"b",label = 'Partícula 2',**kwargs)
        ax.set_ylabel(f'V_{key}')
        ax.set_xlabel('t')
        ax.set_title(f'Velocidad en {key}')

    else:
      fig, axes = plt.subplots(1,len(keys),figsize = (13,5),dpi = 150)
      if len(keys)==1:
        axes = [axes]
      for key,ax in zip(keys,axes):
        ax.plot(self.time,vel1_trans[dict[key]],"k",label = 'Partícula 1',**kwargs)
        ax.plot(self.time,vel2_trans[dict[key]],"b",label = 'Partícula 2',**kwargs)
        ax.set_ylabel(f'V_{key}')
        ax.set_xlabel('t')
        ax.set_title(f'Velocidad en {key}')


    plt.legend()
    plt.tight_layout()

    if save:
      date = datetime.now()
      str = date.strftime("%d_%m_%y_%H%M%S")
      plt.savefig(f'Velocities_{str}.png',bbox_inches ='tight')


  def show_acel(self,*args,save = False,**kwargs):
    ''' 
    Muestra una gráfica de las componentes de la aceleración de cada partícula.

    Si no se pasa ningún argumento muestra las 3 componentes en un subplot, si se pasa un argumento de la forma simulate.show_acel("x") o simulate.show_acel("x","z") muestra sólo las componentes "x" o "x" y "z" de la aceleración.

    Parámetros
    ---------
    save : bool, opcional
      establece si se guarda un .png de la gráfica en el directorio (defecto es False)
    '''
    #exactamente el mismo procedimiento de show_pos
    dict = {"x":0,"y":1,"z":2}
    acel1_trans = list(zip(*self.acel1)) #transpone los arrays con las aceleraciones para ser más fácil de trabajar
    acel2_trans = list(zip(*self.acel2))

    keys = []

    for key in list(set(args).intersection(dict.keys())):
      keys += [key]

    if len(keys)==0:
      keys = ['x','y','z']
      fig, axes = plt.subplots(1,3,figsize = (13,5),dpi = 150)
      for key,ax in zip(keys,axes):
        ax.plot(self.time,acel1_trans[dict[key]],"k",label = 'Partícula 1',**kwargs)
        ax.plot(self.time,acel2_trans[dict[key]],"b",label = 'Partícula 2',**kwargs)
        ax.set_ylabel(f'a_{key}')
        ax.set_xlabel('t')
        ax.set_title(f'Aceleración en {key}')

    else:
      fig, axes = plt.subplots(1,len(keys),figsize = (13,5),dpi = 150)
      if len(keys)==1:
        axes = [axes]
      for key,ax in zip(keys,axes):
        ax.plot(self.time,acel1_trans[dict[key]],"k",label = 'Partícula 1',**kwargs)
        ax.plot(self.time,acel2_trans[dict[key]],"b",label = 'Partícula 2',**kwargs)
        ax.set_ylabel(f'a_{key}')
        ax.set_xlabel('t')
        ax.set_title(f'Aceleración en {key}')

    plt.legend()
    plt.tight_layout()

    if save:
      date = datetime.now()
      str = date.strftime("%d_%m_%y_%H%M%S")
      plt.savefig(f'Aceleration_{str}.png',bbox_inches ='tight')

  def show_traject(self,save = False,**kwargs):
    ''' 
    Muestra una gráfica de las trayectorias de ambas partículas en el espacio R3.

    Parámetros
    ---------
    save : bool, opcional
      establece si se guarda un .png de la gráfica en el directorio (defecto es False)
    '''

    fig = plt.figure(figsize = (13,5),dpi = 150)
    ax = fig.add_subplot(111,projection='3d')

    ax.plot(*list(zip(*self.pos1)),"k", label='Partícula 1',**kwargs)
    ax.plot(*list(zip(*self.pos2)),"b",label='Partícula 2',**kwargs)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_title('Trayectoria de las partículas')

    plt.legend()
    plt.tight_layout()
    if save:
      date = datetime.now()
      str = date.strftime("%d_%m_%y_%H%M%S")
      plt.savefig(f'Trajectory_{str}.png',bbox_inches ='tight')
    